#
# ~/.bashrc
#

export EDITOR=nvim


# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias grep='grep --color=auto'
PS1='[\u@\h \W]\$ '
alias ll='ls -lav --ignore=..'   # show long listing of all except ".."
alias l='ls -lav --ignore=.?*'   # show long listing but no hidden dotfiles except "."
alias cls="clear"
alias clewar="clear"
alias install="sudo pacman -S"
alias search="sudo pacman -Ss"
alias update="sudo pacman -Syu"
alias screenkey="screenkey -s small -p bottom --opacity 0.2 --bg-color black --bak-mode normal -t 0.8"
alias remove="sudo pacman -R"
alias open="xdg-open"
alias clwaer="clear"
alias cleawr="clear"
alias v="nvim"
alias neofetch="neofetch --kitty"
alias webcam='ffplay -window_title -fast -crop 1080*720  -max_delay 0.1 -video_size 1080*720 -framerate 60  /dev/video0 -vf "scale=1280:720:force_original_aspect_ratio=decrease,pad=1280:720:-1:-1:color=black"'

(cat ~/.cache/wal/sequences &)
export PATH=$PATH:~/.local/src/bin/dwmblocks/



